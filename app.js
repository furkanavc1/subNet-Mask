function calculate() {
    'use strict';
    var input1 = document.getElementById("inp-1").value;
    var input2 = document.getElementById("inp-2").value;
    var input3 = document.getElementById("inp-3").value;
    var input4 = document.getElementById("inp-4").value;
    var slashNot = document.getElementById("slash-notation").value;

if (
    (input1 >= 0 && input1 <= 255) &&
        (input3 >= 0 && input3 <= 255) &&
        (input3 >= 0 && input3 <= 255) &&
        (input4 >= 0 && input4 <= 255) &&
        (slashNot >= 0 && slashNot <= 32)
) {
    document.getElementById('resIP').innerHTML = input1 + "." + input3 + "." + input3 + "." + input4;

    var ipBin = {};
    ipBin[1]= parseInt(input1, 10).toString(2)
    ipBin[2]= parseInt(input3, 10).toString(2)
    ipBin[3]= parseInt(input3, 10).toString(2)
    ipBin[4]= parseInt(input4, 10).toString(2)

    var standartClass = "";
    if (input1 <= 126) {
        standartClass = "A";
    } else if (input1 === 127) {
        standartClass = "loopback IP";
    } else if (input1 >= 128 && input1 <= 191) {
        standartClass = "B";
    } else if (input1 >= 192 && input1 <= 223) {
        standartClass = "C";
    } else if (input1 >= 224 && input1 <= 239) {
        standartClass = "D (Multicast Address)";
    } else if (input1 >= 240 && input1 <= 225) {
        standartClass = "E (Experimental)";
    } else {
        standartClass = "Out of range";
    }

    var mask = slashNot;
    var importantBlock = Math.ceil(mask / 8);
    var importantBlockBinary = ipBin[importantBlock];
    var maskBinaryBlockCount = mask % 8;
    if (maskBinaryBlockCount === 0) {
        importantBlock++;
    }
    var maskBinaryBlock = "";
    var maskBlock = "";
    var i;
    for (i = 1; i <= 8; i++) {
        if (maskBinaryBlockCount >= i) {
            maskBinaryBlock += "1";
        } else {
            maskBinaryBlock += "0";
        }
    }
    maskBlock  = parseInt(maskBinaryBlock, 2);

    var netBlockBinary = "";
    var bcBlockBinary = "";
    var i;
    for (i = 1; i <= 8; i++) {
        if (maskBinaryBlock.substr(i - 1, 1) === "1") {
            netBlockBinary += importantBlockBinary.substr(i - 1, 1);
            bcBlockBinary += importantBlockBinary.substr(i - 1, 1);
        } else {
            netBlockBinary += "0";
            bcBlockBinary += "1";
        }
    }

    var mask = "";
    var maskBinary = "";
    var net = "";
    var bc = "";
    var netBinary = "";
    var bcBinary = "";
    var rangeA = "";
    var rangeB = "";
    var i = 1;
    for (i = 1; i <= 4; i++) {
        if (importantBlock > i) {
            mask += "255";
            maskBinary += "11111111";
            netBinary += ipBin[i];
            bcBinary += ipBin[i];
            net += parseInt(ipBin[i], 2);
            bc += parseInt(ipBin[i], 2);
            rangeA += parseInt(ipBin[i], 2);
            rangeB += parseInt(ipBin[i], 2);
        } else if (importantBlock === i) {
            mask += maskBlock;
            maskBinary += maskBinaryBlock;
            netBinary += netBlockBinary;
            bcBinary += bcBlockBinary;
            net += parseInt(netBlockBinary, 2);
            bc += parseInt(bcBlockBinary, 2);
            rangeA += (parseInt(netBlockBinary, 2) + 1);
            rangeB += (parseInt(bcBlockBinary, 2) - 1);
        } else {
            mask += 0;
            maskBinary += "00000000";
            netBinary += "00000000";
            bcBinary += "11111111";
            net += "0";
            bc += "255";
            rangeA += 0;
            rangeB += 255;
        }
        if (i < 4) {
            mask += ".";
            maskBinary += ".";
            netBinary += ".";
            bcBinary += ".";
            net += ".";
            bc += ".";
            rangeA += ".";
            rangeB += ".";
        }
    }
    document.getElementById('resMask').innerHTML = mask;
    document.getElementById('resNet').innerHTML = net;
    document.getElementById('resBC').innerHTML = bc;
    document.getElementById('resRange').innerHTML = rangeA + " - " + rangeB;
    document.getElementById('resBinIP').innerHTML = ipBin[1] + "." + ipBin[2] + "." + ipBin[3] + "." + ipBin[4];
    document.getElementById('resBinMask').innerHTML = maskBinary;
    document.getElementById('resBinNet').innerHTML = netBinary;
    document.getElementById('resBinBC').innerHTML = bcBinary;
    document.getElementById('resClass').innerHTML = standartClass;
} else {
    alert("invalid value");
}
}
